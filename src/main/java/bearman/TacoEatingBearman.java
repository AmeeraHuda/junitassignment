package bearman;

import food.Food;

public class TacoEatingBearman extends Bearman{
	
	public void eat(Food food3) {
		food3.feedBearman(this);
	}
}
